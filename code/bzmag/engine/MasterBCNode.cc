#include "MasterBCnode.h"
#include "expression.h"

#include "core/simplepropertybinder.h"
#include "core/enumpropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(MasterBCNode, BCNode);


//----------------------------------------------------------------------------
MasterBCNode::MasterBCNode() : direction_(true)
{

}

//----------------------------------------------------------------------------
MasterBCNode::~MasterBCNode()
{

}

//----------------------------------------------------------------------------
void MasterBCNode::bindProperty()
{
    BIND_PROPERTY(bool, Orientation, &setOrientation, &orientation);
}

