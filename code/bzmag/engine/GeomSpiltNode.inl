
//-----------------------------------------------------------------------------
inline void GeomSpiltNode::setPlane(const SPILT_PLANE& plane)
{
    plane_ = plane;
}

//-----------------------------------------------------------------------------
inline const GeomSpiltNode::SPILT_PLANE& GeomSpiltNode::getPlane() const
{
    return plane_;
}

//-----------------------------------------------------------------------------
inline void GeomSpiltNode::setOrientation(bool o)
{
    selectd_plane_ = o;
}

//-----------------------------------------------------------------------------
inline bool GeomSpiltNode::getOrientation() const
{
    return selectd_plane_;
}

//-----------------------------------------------------------------------------
inline String GeomSpiltNode::description() const
{
    return "SpiltBody";
}
