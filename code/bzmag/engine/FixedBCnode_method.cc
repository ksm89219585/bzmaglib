#include "FixedBCnode.h"
#include "core/methodbinder.h"

using namespace bzmag;
using namespace bzmag::engine;

//----------------------------------------------------------------------------
static void FixedBCNode_d_getBCValue_v(FixedBCNode* self, Parameter* param)
{
    float64 value = self->getBCValue();
    param->out()->get<float64>(0) = value;
}

//----------------------------------------------------------------------------
void FixedBCNode::bindMethod()
{
    BIND_METHOD(d_getBCValue_v, FixedBCNode_d_getBCValue_v);
}
