#include "SimpleGeometry.h"


#include "print_utils.h"

using namespace bzmag;
using namespace bzmag::engine;

//-----------------------------------------------------------------------------
SimpleGeometry::SimpleGeometry() : ref_bcs_(nullptr)//, based_points_(nullptr)
{

}

//-----------------------------------------------------------------------------
SimpleGeometry::~SimpleGeometry()
{

}

//-----------------------------------------------------------------------------
void SimpleGeometry::clear()
{
    polygons_.clear();
    holes_.clear();

    seg_polygons_.clear();
    seg_holes_.clear();

    ref_bcs_ = nullptr;
    //based_points_ = nullptr;
}

//-----------------------------------------------------------------------------
void SimpleGeometry::setPolyset(Polygon_set_2& geometry)
{
    clear();

    // polygons_with_holes --> 리스트 형태의 polygon_with_holes로 변환
    std::list<Polygon_with_holes_2> res;
    geometry.polygons_with_holes(std::back_inserter(res));

    std::list<Polygon_with_holes_2>::const_iterator it;
    for (it = res.begin(); it != res.end(); ++it)
    {
        const Polygon_with_holes_2& polyhole = *it;

        Curves curves;
        extract_curves(polyhole, curves);

        int32 tag = (int32)std::distance(res.cbegin(), it);
        make_polygons(curves, tag);
    }


}

//-----------------------------------------------------------------------------
void SimpleGeometry::extract_curves(const Polygon_with_holes_2& polyhole, Curves& curves)
{
    // 외곽경계 추출
    const Polygon_2& outer = polyhole.outer_boundary();

    // 경계로부터 Point 추출
    extract_curves(outer, curves);

    // 내부 홀들로 부터 Point 추출
    Polygon_with_holes_2::Hole_const_iterator hit;
    for (hit = polyhole.holes_begin(); hit != polyhole.holes_end(); ++hit)
    {
        const Polygon_2& inner = *hit;
        extract_curves(inner, curves);
    }
}

//-----------------------------------------------------------------------------
void SimpleGeometry::extract_curves(const Polygon_2& poly, Curves& curves)
{
    Polygon_2::Curve_const_iterator it;
    for (it = poly.curves_begin(); it != poly.curves_end(); ++it)
    {
        const X_monotone_curve_2& curve = *it;
        curves.push_back(curve);
    }
}

//-----------------------------------------------------------------------------
void SimpleGeometry::make_polygons(const Curves& curves, int32 tag)
{
    Arrangement arr;
    CGAL::insert(arr, curves.begin(), curves.end());
    Arrangement::Face_const_iterator ff;
    for (ff = arr.faces_begin(); ff != arr.faces_end(); ++ff) {
        Arrangement::Face face = *ff;

        if (face.has_outer_ccb())
        {
            std::list<X_monotone_curve_2> poly;
            Arrangement::Ccb_halfedge_const_circulator curr = ff->outer_ccb();
            X_monotone_curve_2 curve = curr->curve();
            poly.push_back(curve);
            Traits_2::Point_2 ss = curve.source();
            Traits_2::Point_2 tt = curve.target();
            curr++;

            while (curr != ff->outer_ccb()) {
                curve = curr->curve();

                // 이전 끝점이 지금 커브의 시작점이면
                if (tt == curve.source())
                    poly.push_back(curve);

                // 지금커브의 끝점이 이전커브의 시작점이면
                else
                    poly.push_front(curve);

                ss = curve.source();
                tt = curve.target();
                ++curr;
            }

            Polygon_2 polygon(poly.begin(), poly.end());
            //print_polygon(polygon);
            if (polygon.orientation() == CGAL::COUNTERCLOCKWISE) {
                polygons_.push_back(Polygon_tag(polygon, tag));
            }
            else {
                polygon.reverse_orientation();
                holes_.push_back(Polygon_tag(polygon, tag));
            }
        }
    }
}

//-----------------------------------------------------------------------------
bool SimpleGeometry::insertPoints(std::list<Traits_2::Point_2>& points)
{
    //based_points_ = &points;

    Polygons::iterator it;
    for (it = polygons_.begin(); it != polygons_.end(); ++it)
    {
        Polygon_2 &poly = it->first;
        refine_polygon_with_the_points(poly, points);
    }

    for (it = holes_.begin(); it != holes_.end(); ++it)
    {
        Polygon_2 &poly = it->first;
        refine_polygon_with_the_points(poly, points);
    }

    return true;
}

//-----------------------------------------------------------------------------
void SimpleGeometry::refine_polygon_with_the_points(Polygon_2 &poly,
    std::list<Traits_2::Point_2>& points)
{
    Arrangement arr;
    CGAL::insert(arr, poly.curves_begin(), poly.curves_end());

    // 커브들이 주어진 점들과 만나는 <커브, 점> 셋트 찾는다
    std::list<Query_result> results;
    locate(arr, points.begin(), points.end(), std::back_inserter(results));

    // 검색된 커브를 점으로 쪼갠다
    std::list<Query_result>::const_iterator it;
    for (it = results.begin(); it != results.end(); ++it) {
        if (const Halfedge_const_handle* e =
            boost::get<Halfedge_const_handle>(&(it->second))) // on an edge
        {
            Halfedge_handle eh = arr.non_const_handle(*e);
            const X_monotone_curve_2& curve = (*e)->curve();
            X_monotone_curve_2 c1, c2;
            Traits_2::Point_2 pt = it->first;
            curve.split(pt, c1, c2);
            arr.split_edge(eh, c1, c2);
        }
    }

    // 쪼개진 엣지로부터 새로운 폴리곤을 만든다
    Arrangement::Face_const_iterator ff;
    for (ff = arr.faces_begin(); ff != arr.faces_end(); ++ff)
    {
        Arrangement::Face face = *ff;
        std::list<X_monotone_curve_2> new_poly;
        if (face.has_outer_ccb())
        {
            Arrangement::Ccb_halfedge_const_circulator curr = ff->outer_ccb();
            X_monotone_curve_2 curve = curr->curve();
            new_poly.push_back(curve);
            Traits_2::Point_2 ss = curve.source();
            Traits_2::Point_2 tt = curve.target();
            curr++;

            while (curr != ff->outer_ccb()) {
                curve = curr->curve();

                // 이전 끝점이 지금 커브의 시작점이면
                if (tt == curve.source())
                    new_poly.push_back(curve);

                // 지금커브의 끝점이 이전커브의 시작점이면
                else
                    new_poly.push_front(curve);

                ss = curve.source();
                tt = curve.target();
                ++curr;
            }

            poly.clear();
            poly.init(new_poly.begin(), new_poly.end());
            return;
        }
    }
}

//-----------------------------------------------------------------------------
void SimpleGeometry::segmentation(float64 based_length, float64 based_angle)
{
    Polygons::const_iterator it;
    for (it = polygons_.begin(); it != polygons_.end(); ++it) {
        const Polygon_2& poly = it->first;
        Polygon_2 seg_poly;
        segment_polygon(poly, seg_poly, based_length, based_angle);
        seg_polygons_.push_back(Polygon_tag(seg_poly, it->second));
    }

    for (it = holes_.begin(); it != holes_.end(); ++it) {
        const Polygon_2& poly = it->first;
        Polygon_2 seg_poly;
        //poly.reverse_orientation();
        segment_polygon(poly, seg_poly, based_length, based_angle);
        seg_holes_.push_back(Polygon_tag(seg_poly, it->second));
    }
}

//----------------------------------------------------------------------------
// 참고) BC에서 참조하는 X_monoton_curve_2 는 세그멘테이션 이전임
void SimpleGeometry::linkBC(std::list<const BCNode*>* bc_nodes)
{
    ref_bcs_ = bc_nodes;
}

//----------------------------------------------------------------------------
void SimpleGeometry::segment_polygon(const Polygon_2& source, Polygon_2& result, float64 based_length, float64 based_angle)
{
    Polygon_2::Curve_const_iterator cit;
    for (cit = source.curves_begin(); cit != source.curves_end(); ++cit)
    {
        const X_monotone_curve_2& edge = *cit;

        // 경계커브 테스트는 반드시 세그멘테이션 이전 정보를 활용해야 함!
        bool boundary = is_boundary_curve(edge);
        std::list<X_monotone_curve_2> seg_edges;
        if (boundary) {
            segment_curve(edge, seg_edges, based_length, based_angle * 2, true);
        }
        else {
            segment_curve(edge, seg_edges, based_length, based_angle, false);
        }

        result.insert(seg_edges.begin(), seg_edges.end());
    }
}

//-----------------------------------------------------------------------------
bool SimpleGeometry::is_boundary_curve(const X_monotone_curve_2& edge) const
{
    if (ref_bcs_ == nullptr) return false;

    std::list<const BCNode*>::iterator it;
    for (it = ref_bcs_->begin(); it != ref_bcs_->end(); ++it)
    {
        const BCNode* bc = *it;
        if (bc->testCurve(edge)) {
            return true;
        }
    }

    return false;
}
