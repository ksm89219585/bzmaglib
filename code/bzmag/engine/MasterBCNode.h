#pragma once

/*
Description : Master Periodic Boundary condition Node
Last Update : 2021.01.05
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "bcnode.h"

namespace bzmag
{
namespace engine
{
    class MasterBCNode : public BCNode
    {
    public:
        MasterBCNode();
        virtual ~MasterBCNode();
        DECLARE_CLASS(MasterBCNode, BCNode);

    public:
        void setOrientation(bool dir);
        bool orientation() const;

    public:
        static void bindMethod();
        static void bindProperty();

    private:
        // curve direction ;  origin/reverse
        bool direction_;

    };

#include "MasterBCnode.inl"

}
}