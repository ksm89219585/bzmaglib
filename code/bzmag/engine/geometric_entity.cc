#include "Geometric_entity.h"

namespace bzmag
{
namespace engine
{

    //----------------------------------------------------------------------------
    void segment_curve(const X_monotone_curve_2& edge, std::list<X_monotone_curve_2>& result, float64 based_length, float64 based_angle, bool segment_line)
    {
        // 커브의 시작점 끝점
        const Traits_2::Point_2& ss = edge.source();
        const Traits_2::Point_2& tt = edge.target();

        float64 ssx = CGAL::to_double(ss.x());
        float64 ssy = CGAL::to_double(ss.y());
        float64 ttx = CGAL::to_double(tt.x());
        float64 tty = CGAL::to_double(tt.y());

        // 곡선이면, 무조건 세그멘테이션
        if (edge.is_circular())
        {
            // 커브의 서포팅 원
            const Circle_2& circle = edge.supporting_circle();
            Point_2 cc = circle.center();
            float64 ccx = CGAL::to_double(cc.x());
            float64 ccy = CGAL::to_double(cc.y());
            float64 radii = std::sqrt(CGAL::to_double(circle.squared_radius()));

            float64 sAngle = std::atan2(ssy - ccy, ssx - ccx);
            float64 eAngle = std::atan2(tty - ccy, ttx - ccx);

            float64 dAngle = eAngle - sAngle;
            if (edge.orientation() == CGAL::COUNTERCLOCKWISE) {
                if (dAngle < 0) dAngle += (2 * CGAL_PI);
            }
            else {
                if (dAngle > 0) dAngle -= (2 * CGAL_PI);
            }

            // 세그먼트수는 아크를 10도간격으로 잘랐을때의 세그먼트 숫자와
            // 주어진 길이로 잘랐을때의 세그먼트의 숫자 중
            // 큰 것을 이용한다
            float64 arc_length = radii * abs(dAngle);

            int num_seg1 = int(arc_length / based_length + 0.5);
            if (num_seg1 == 0) num_seg1 = 1;
            int num_seg2 = int(abs(dAngle)*180.0 / CGAL_PI / based_angle + 0.5);

            // 세그먼트 수
            int num_seg = std::max(num_seg1, num_seg2);

            // 세그먼트 수를 짝수개로 조정한다
            if ((num_seg % 2) == 1) num_seg = num_seg + 1;

            // 세그먼트 각도 ; 1도 근처가 될 것임
            float64 delAngle = dAngle / num_seg;


            // 세그멘테이션의 기저 절점
            std::vector<float64> seg_xs(num_seg + 1), seg_ys(num_seg + 1);

            // 시작점
            seg_xs[0] = ssx;
            seg_ys[0] = ssy;

            // 세그멘테이션 절점
            for (int i = 1; i < num_seg; ++i) {
                // 정방향 (절반값)
                float64 p1x = ccx + radii * cos(sAngle + i * delAngle) * 0.5;
                float64 p1y = ccy + radii * sin(sAngle + i * delAngle) * 0.5;

                // 역방향 (절반값)
                float64 p2x = ccx + radii * cos(eAngle - i * delAngle) * 0.5;
                float64 p2y = ccy + radii * sin(eAngle - i * delAngle) * 0.5;


                seg_xs[i] = seg_xs[i] + p1x;
                seg_ys[i] = seg_ys[i] + p1y;
                seg_xs[num_seg - i] = seg_xs[num_seg - i] + p2x;
                seg_ys[num_seg - i] = seg_ys[num_seg - i] + p2y;

            }

            // 끝점
            seg_xs[num_seg] = ttx;
            seg_ys[num_seg] = tty;

            // 실제 커브를 만든다
            for (int i = 0; i < num_seg; ++i) {
                Point_2 p1(seg_xs[i], seg_ys[i]);
                Point_2 p2(seg_xs[i + 1], seg_ys[i + 1]);
                X_monotone_curve_2 segmented_edge(p1, p2);
                result.emplace_back(segmented_edge);
            }
        }

        // 직선인 경우
        //boundary edge 이면 세그멘테이션 하고, 그렇지 않으면 세그멘테이션 하지 않는다.
        else
        {
            if (segment_line)
            {
                // 벡터 길이
                float64 line_length = sqrt((ssx - ttx)*(ssx - ttx) + (ssy - tty)*(ssy - tty));
                int num_seg = (int)(line_length / (2.0*based_length));
                if (num_seg < 1) num_seg = 1;

                // 짝수로 만든다
                if ((num_seg % 2) == 1) num_seg = num_seg + 1;

                // 세그멘테이션의 기저 절점
                std::vector<float64> seg_xs(num_seg + 1), seg_ys(num_seg + 1);

                // 시작점
                seg_xs[0] = ssx;
                seg_ys[0] = ssy;

                // 라인을 세그멘테이션 한다
                for (int i = 1; i < num_seg; ++i) {
                    seg_xs[i] = ssx + (ttx - ssx)*((float64)i / (float64)num_seg);
                    seg_ys[i] = ssy + (tty - ssy)*((float64)i / (float64)num_seg);
                }

                // 끝점
                seg_xs[num_seg] = ttx;
                seg_ys[num_seg] = tty;

                // 실제 커브를 만든다
                for (int i = 0; i < num_seg; ++i) {
                    Point_2 p1(seg_xs[i], seg_ys[i]);
                    Point_2 p2(seg_xs[i + 1], seg_ys[i + 1]);
                    X_monotone_curve_2 segmented_edge(p1, p2);
                    result.emplace_back(segmented_edge);
                }

            }
            else {
                X_monotone_curve_2 segmented_edge(Point_2(ssx, ssy), Point_2(ttx, tty));
                result.emplace_back(segmented_edge);
            }
        }
    }

    //-----------------------------------------------------------------------------
    void print_polygon(const Polygon_2& poly)
    {
        std::cout << "Polygon : ";
        Polygon_2::Curve_const_iterator cc;
        for (cc = poly.curves_begin(); cc != poly.curves_end(); ++cc) {
            std::cout << "([" << (*cc).source() << "]-->[" << (*cc).target() << "]),";
        }
        if (poly.orientation() == CGAL::CLOCKWISE) {
            std::cout << "Hole" << std::endl;
        }
        else {
            std::cout << "Boundary" << std::endl;
        }
    }

}
}