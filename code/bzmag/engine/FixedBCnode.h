#pragma once

/*
Description : Fixed Boundary condition Node
Last Update : 2021.01.05
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "bcnode.h"
#include "core/ref.h"

namespace bzmag
{
namespace engine
{
    class Expression;
    class FixedBCNode : public BCNode
    {
    public:
        FixedBCNode();
        virtual ~FixedBCNode();
        DECLARE_CLASS(FixedBCNode, BCNode);

    public:
        void setBCValue(const String& value);
        const String& getBCValue_as_String() const;
        float64 getBCValue();

    public:
        static void bindMethod();
        static void bindProperty();

    protected:
        String svalue_;
        Ref<Expression> value_;
    };

#include "FixedBCnode.inl"

}
}