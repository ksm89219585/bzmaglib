#define _USE_MATH_DEFINES

#include "BCnode.h"
#include "GeomHeadNode.h"
#include "geom_to_path.h"
#include "core/methodbinder.h"

using namespace bzmag;
using namespace bzmag::engine;

//----------------------------------------------------------------------------
static void BCNode_v_addCurve_ni(BCNode* self, Parameter* param)
{
    GeomHeadNode* head = param->in()->get<GeomHeadNode*>(0).get();
    if (!head) {
        std::cout << "Given node is not a GeomHeadNode!" << std::endl;
        return;
    }

    int32 idx = param->in()->get<int32>(1);
    const GeomBaseNode::Curves& curves = head->getCurves();
    std::cout << "The Object has " << curves.size() << " edges." << std::endl;
    std::cout << "Edge which index number is " << idx << " added as boundary!" << std::endl;
    if (idx < (int32)curves.size()) {
        const X_monotone_curve_2& curve = curves[idx];
        self->addBoundary(curve);


        std::cout << "Boundary added! There are " << self->getNumberOfCurves() << " curves in the BC." << std::endl;
    }
}

//----------------------------------------------------------------------------
static void BCNode_z_getPath_v(BCNode* self, Parameter* param)
{
    param->out()->clear();

    GeomToPath geom_to_path(self);
    GeomToPath::VertexList vertices;
    geom_to_path.makePath(vertices);

    GeomToPath::VertexList::const_iterator it;
    for (it = vertices.begin(); it != vertices.end(); ++it)
    {
        GeomToPath::VertexInfo pt = (*it);
        param->out()->add<float64>(pt.x);
        param->out()->add<float64>(pt.y);
        param->out()->add<uint32>(pt.cmd);
    }
}

//----------------------------------------------------------------------------
static void BCNode_d_getDistanceFromtheOrigin_ddb(BCNode* self, Parameter* param)
{
    float64 x = param->in()->get<float64>(0);
    float64 y = param->in()->get<float64>(1);
    bool reverse = param->in()->get<bool>(2);

    float64 distance = self->getDistanceFromtheOrigin(x, y, reverse);
    param->out()->get<float64>(0) = distance;
}

//----------------------------------------------------------------------------
void BCNode::bindMethod()
{
    BIND_METHOD(v_addCurve_ni, BCNode_v_addCurve_ni);
    BIND_METHOD(z_getPath_v, BCNode_z_getPath_v);
    BIND_METHOD(d_getDistanceFromtheOrigin_ddb, BCNode_d_getDistanceFromtheOrigin_ddb);
}
