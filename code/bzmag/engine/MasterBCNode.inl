
//-----------------------------------------------------------------------------
inline void MasterBCNode::setOrientation(bool dir)
{
    direction_ = dir;
}

//-----------------------------------------------------------------------------
inline bool MasterBCNode::orientation() const
{
    return direction_;
}

