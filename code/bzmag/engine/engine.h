#pragma once

#include "geomheadnode.h"

#include "GeomPrimitiveNode.h"
#include "geomcurvenode.h"
#include "geomcirclenode.h"
#include "geomrectnode.h"
#include "geombandnode.h"
#include "geomclonefromnode.h"

#include "geomclonetonode.h"
#include "geomcoverlinenode.h"
#include "geommovenode.h"
#include "geomrotatenode.h"
#include "geomspiltnode.h"

#include "geomsubtractnode.h"
#include "GeomUniteNode.h"
#include "GeomIntersectionNode.h"

#include "CSnode.h"

#include "BCnode.h"
#include "MasterBCnode.h"
#include "SlaveBCnode.h"
#include "FixedBCnode.h"


#include "materialnode.h"
#include "coilnode.h"
#include "windingnode.h"
#include "expressionserver.h"

#include "GeometryToTriangle.h"

#include "core/define.h"
#include "core/module.h"
#include "core/kernel.h"