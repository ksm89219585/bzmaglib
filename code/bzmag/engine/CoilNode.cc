#include "coilnode.h"
#include "GeomHeadNode.h"
#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(CoilNode, Node);

//----------------------------------------------------------------------------
CoilNode::CoilNode() : ref_node_(nullptr), direction_(true), turns_(1)
{

}

//----------------------------------------------------------------------------
CoilNode::~CoilNode()
{

}

//----------------------------------------------------------------------------
void CoilNode::setReferenceNode(GeomHeadNode* node)
{
    ref_node_ = node;
}

//----------------------------------------------------------------------------
GeomHeadNode* CoilNode::getReferenceNode() const
{
    return ref_node_;
}

//----------------------------------------------------------------------------
void CoilNode::clear()
{
    ref_node_ = nullptr;
    direction_ = true;
    turns_ = 1;
}

//----------------------------------------------------------------------------
bool CoilNode::update()
{
    return true;
}

//----------------------------------------------------------------------------
void CoilNode::onAttachTo(Node* parent)
{

}


//----------------------------------------------------------------------------
void CoilNode::onDetachFrom(Node* parent)
{

}


//----------------------------------------------------------------------------
void CoilNode::bindProperty()
{
    BIND_PROPERTY(bool, Direction, &setDirection, &getDirection);
    BIND_PROPERTY(int32, Turns, &setNumberOfTurns, &getNumberOfTurns);
}

