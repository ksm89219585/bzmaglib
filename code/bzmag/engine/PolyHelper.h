#pragma once

#include "geometric_entity.h"
#include "SimpleGeometry.h"
#include "BCnode.h"
#include <CGAL/Constrained_Delaunay_triangulation_2.h>

namespace bzmag
{
namespace engine
{

    // 하나의 도메인(GeomHeadNode가 참고하는 Polyset)을 관리하는 핼퍼
    class PolyHelper
    {
    public:
        typedef std::pair<Polygon_2, Traits_2::Point_2>        SubDomain;
        typedef std::list<SubDomain>                           SubDomains;
        typedef CGAL::Constrained_Delaunay_triangulation_2<LK>  CDT;
        typedef CDT::Finite_faces_iterator                     Face_circulator;
        typedef CDT::Point                                     Point;
        typedef std::list<Traits_2::Point_2>                   Pts;

    public:
        PolyHelper();
        ~PolyHelper();

        // 1. ID와 폴리셋 셋팅 ; 주어진 Polyset은 SimpleGeometry class에 할당되며
        //                       Simple Geometry(최소한의 단위유닛)가 된다
        void setPolyset(Polygon_set_2& polyset, int ID);

        // 2. 모델링 내의 모든 절점을 고려해 커브 나누기(해당 커브만)
        void insertPoints(std::list<Traits_2::Point_2>& points);

        // 3. Triangle 의 입력을 만들기 위해 Simple Geometry와 
        //    이를 특정할 수 있는 Geometry내의 임의점을 찾아 Subdomain으로 만든다
        //    동시에 세그멘테이션 되며 이를 통해 도메인(폴리셋)의 면적이 구해진다
        //    참고로 경계조건을 고려하여 직선의 세그멘테이션을 실시한다.
        void configSubdomains(float64 based_length, float64 based_angle, std::list<const BCNode*>* bc_nodes);

        // 면적얻기
        float64 area() const { return area_; }

        // ID얻기
        int ID() const { return ID_; }

        // hitTest
        bool hitTest(const Traits_2::Point_2& pt);

        // Subdomain 얻기
        SubDomains::iterator firstSubDomain() { return subdomains_.begin(); };
        SubDomains::iterator lastSubDomain() { return subdomains_.end(); };

        SubDomains::iterator firstHoleDomain() { return holedomains_.begin(); };
        SubDomains::iterator lastHoleDomain() { return holedomains_.end(); };

        void clear();

    protected:
        // 주어진 폴리곤 (내부적인 홀을 고려한) 내부의 임의점 찾기
        // Triangulation 이후 요소 중앙점 테스트를 통해 알아냄
        bool get_arbitrary_point_in_polygon(const Polygon_set_2& polyset, Traits_2::Point_2& pt);

        // 면적구하기
        float64 calculate_area(Polygon_set_2& polyset);

        // Triangulation을 위한 기저점 추출 ; segmentation 후에 호출되어야 함
        void extract_based_points_from_polyset(const Polygon_set_2& polyset, std::list<Point>& vertices);
        void extract_based_points_from_polyholes(const Polygon_with_holes_2& polyhole, std::list<Point>& vertices);
        void extract_based_points_from_polygon(const Polygon_2& polygon, std::list<Point>& vertices);

    private:
        int ID_;
        SimpleGeometry polygon_;

        // 세그멘테이션 된 것임
        SubDomains     subdomains_;

        // 세그멘테이션 된 것임
        SubDomains     holedomains_;

        // 도메인 면적
        double area_;

        // Hole을 지정할 수 있는 임의의 점
        Pts hole_pts_;
    };


}
}