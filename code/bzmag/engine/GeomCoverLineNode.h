#pragma once

/*
Description : CoverLine Node
Last Update : 2017.09.28
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "geombasenode.h"

namespace bzmag
{
namespace engine
{
    class GeomCoverLineNode : public GeomBaseNode
    {
    public:
        GeomCoverLineNode();
        virtual ~GeomCoverLineNode();
        DECLARE_CLASS(GeomCoverLineNode, GeomBaseNode);

    public:
        // 이하 재정의 되어야 함
        virtual Transformation getMyTransform();
        virtual String description() const;

    protected:
        virtual bool make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform);
        virtual void updateCovered();

    public:
        static void bindMethod();
        static void bindProperty();
    };

#include "geomcoverlinenode.inl"

}
}