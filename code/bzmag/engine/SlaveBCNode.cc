#include "SlaveBCnode.h"
#include "expression.h"

#include "core/simplepropertybinder.h"
#include "core/enumpropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(SlaveBCNode, MasterBCNode);


//----------------------------------------------------------------------------
SlaveBCNode::SlaveBCNode() : even_(true), ref_(nullptr)
{

}

//----------------------------------------------------------------------------
SlaveBCNode::~SlaveBCNode()
{

}

//-----------------------------------------------------------------------------
void SlaveBCNode::setReference(Node* ref)
{
    MasterBCNode* bc = dynamic_cast<MasterBCNode*>(ref);
    ref_ = bc;
}

//-----------------------------------------------------------------------------
Node* SlaveBCNode::getReference() const
{
    Node* bc = dynamic_cast<Node*>(ref_);
    return bc;
}

//----------------------------------------------------------------------------
void SlaveBCNode::bindProperty()
{
    BIND_PROPERTY(bool, EvenRelation, &setEvenRelation, &relation);
    BIND_PROPERTY(Node*, Reference, &setReference, &getReference);
}

