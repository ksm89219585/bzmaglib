#include "FixedBCnode.h"
#include "expression.h"

#include "core/simplepropertybinder.h"
#include "core/enumpropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(FixedBCNode, BCNode);


//----------------------------------------------------------------------------
FixedBCNode::FixedBCNode() : svalue_("0")
{
    uint32 key = getID();
    value_ = new Expression();
    value_->setKey("bc_" + std::to_string(key));
}

//----------------------------------------------------------------------------
FixedBCNode::~FixedBCNode()
{

}

//-----------------------------------------------------------------------------
void FixedBCNode::setBCValue(const String& value)
{
    // 이전값 임시 저장
    const String& pval = value_->getExpression();

    // 표현식 업데이트
    if (!value_->setExpression(value)) {
        value_->setExpression(pval);
        return;
    }

    // 맴버 새로운 값으로 업데이트
    svalue_ = value;
}

//----------------------------------------------------------------------------
const String& FixedBCNode::getBCValue_as_String() const
{
    return svalue_;
}

//----------------------------------------------------------------------------
float64 FixedBCNode::getBCValue()
{
    return value_->eval();
}

//----------------------------------------------------------------------------
void FixedBCNode::bindProperty()
{
    BIND_PROPERTY(const String&, Value, &setBCValue, &getBCValue_as_String);
}

