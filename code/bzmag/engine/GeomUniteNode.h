#pragma once

/*
Description : Abstract Node for Boolean Operation
Last Update : 2020.08.21
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "geombooleannode.h"


namespace bzmag
{
namespace engine
{
    class GeomUniteNode : public GeomBooleanNode
    {
    public:
        GeomUniteNode();
        virtual ~GeomUniteNode();
        DECLARE_CLASS(GeomUniteNode, GeomBooleanNode);

    public:
        virtual String description() const;

    public:
        static void bindMethod();
        static void bindProperty();

    protected:
        virtual void boolean_operation(Polygon_set_2& polyset);
    };

#include "geomunitenode.inl"

}
}