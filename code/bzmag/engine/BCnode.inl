
//-----------------------------------------------------------------------------
inline int32 BCNode::getNumberOfCurves() const
{
    return (int32)(curves_.size());
}