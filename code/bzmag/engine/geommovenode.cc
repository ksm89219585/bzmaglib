#include "geommovenode.h"
#include "geomheadnode.h"
#include "csnode.h"
#include "expression.h"

#include "core/vector2.h"
#include "core/simplepropertybinder.h"
#include "core/nodeeventpublisher.h"

using namespace bzmag;
using namespace bzmag::engine;

IMPLEMENT_CLASS(GeomMoveNode, GeomBaseNode);

//----------------------------------------------------------------------------
GeomMoveNode::GeomMoveNode() : sdx_("0"), sdy_("0")
{
    uint32 key = getID();

    dx_ = new Expression();
    dy_ = new Expression();

    dx_->setKey("dx_" + std::to_string(key));
    dy_->setKey("dy_" + std::to_string(key));
}

//----------------------------------------------------------------------------
GeomMoveNode::~GeomMoveNode()
{

}

//----------------------------------------------------------------------------
bool GeomMoveNode::setParameters(const String& dx, const String& dy)
{
    if (dx_.invalid() || dy_.invalid()) return false;

    if (!dx_->setExpression(dx) || !dy_->setExpression(dy)) {
        dx_->setExpression(sdx_);
        dy_->setExpression(sdy_);

        return false;
    }

    sdx_ = dx;
    sdy_ = dy;

    return update();
}

//----------------------------------------------------------------------------
float64 GeomMoveNode::get_dx() const
{
    Expression* dx = dx_;
    if (dx_.valid())
        return dx->eval();
    else
        return 0;
}

//----------------------------------------------------------------------------
float64 GeomMoveNode::get_dy() const
{
    Expression* dy = dy_;
    if (dy_.valid())
        return dy->eval();
    else
        return 0;
}

//----------------------------------------------------------------------------
bool GeomMoveNode::make_geometry(Polygon_set_2& polyset, Curves& curves, Vertices& vertices, Transformation transform)
{
    return true;
}

//----------------------------------------------------------------------------
Transformation GeomMoveNode::getMyTransform()
{
    // 참조좌표계가 설정되어 있다면, 
    Transformation rotate;
    if (cs_.valid()) {
        float64 angle = cs_->getGlobalAngle();

        NT diry = std::sin(angle) * 256 * 256 * 256;
        NT dirx = std::cos(angle) * 256 * 256 * 256;
        NT sin_alpha;
        NT cos_alpha;
        NT w;
        CGAL::rational_rotation_approximation(dirx, diry,
            sin_alpha, cos_alpha, w,
            NT(1), NT(1000000));

        rotate = Transformation(CGAL::ROTATION, sin_alpha, cos_alpha, w);
    }

    // 실제 이동 될 변위
    Vector_2 disp = rotate(Vector_2(dx_->eval(), dy_->eval()));
    Transformation transform(CGAL::TRANSLATION, disp);
    return transform;
}

//----------------------------------------------------------------------------
void GeomMoveNode::bindProperty()
{
    BIND_PROPERTY(const String&, dx, &set_dx, &get_dx_asString);
    BIND_PROPERTY(const String&, dy, &set_dy, &get_dy_asString);
}
