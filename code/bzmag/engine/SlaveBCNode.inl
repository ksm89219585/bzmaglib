
//-----------------------------------------------------------------------------
inline void SlaveBCNode::setEvenRelation(bool even)
{
    even_ = even;
}

//-----------------------------------------------------------------------------
inline bool SlaveBCNode::relation() const
{
    return even_;
}

