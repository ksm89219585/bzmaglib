#pragma once

/*
Description : Periodic Boundary condition Node
Last Update : 2021.01.05
Author : Jaenam Bae (jaenam@dongyang.ac.kr)
*/

#include "MasterBCNode.h"

namespace bzmag
{
namespace engine
{
    class SlaveBCNode : public MasterBCNode
    {
    public:
        SlaveBCNode();
        virtual ~SlaveBCNode();
        DECLARE_CLASS(SlaveBCNode, MasterBCNode);

    public:
        void setEvenRelation(bool even);
        bool relation() const;

        void setReference(Node* ref);
        Node* getReference() const;


    public:
        static void bindMethod();
        static void bindProperty();

    private:
        // even/odd?
        bool even_;

        // when it is slave type it needs master bc node as reference
        MasterBCNode* ref_;

    };

#include "SlaveBCnode.inl"

}
}